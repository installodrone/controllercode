/***************************************************
  A few labels with different text alignment along with a group of radio 
  buttons and also checkbox

  NOTE: screeen rotation is set to 270 degrees. Don't worry, the touchscreen will map
  correctly.

  Uses Adafruit TFT library for the example:
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

/** NOT FOR USE WITH THE TOUCH SHIELD, ONLY FOR THE BREAKOUT! **/

#include <LinkedList.h>
#include <gfxfont.h>
#include <Adafruit_SPITFT_Macros.h>
#include <Adafruit_SPITFT.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <SPI.h>
#include <RH_RF69.h>
#include "Beacon.h"
#include <Adafruit_RA8875.h>
#include "GuiLibrary.h"
#include <Fonts/FreeSansBold9pt7b.h>

// The display uses hardware SPI, plus #9 & #10
#define RA8875_INT 6
#define RA8875_CS 11
#define RA8875_RESET 10
#define RFM_CS 8
#define RFM_INT 3
#define RFM_RST 4

#define BTN_ON_OFF 12
#define KEEP_ALIVE 13
#define VBATPIN A7

RH_RF69 rf69 = RH_RF69(RFM_CS, RFM_INT);
Adafruit_RA8875 tft = Adafruit_RA8875(RA8875_CS, RA8875_RESET);
// Setup the gui model
// needs reference to the tft and the touchscreen
Gui gui(&tft, RA8875_INT, 0, 0, 800, 480);
//These must be accessed from other files
GuiTimerProgressBar* pbTakeOffTime;
GuiTimerProgressBar* pbGlueTime;
GuiProgressBar* pbArmXAngle;
GuiProgressBar* pbArmYAngle;
GuiProgressBar* pbDistanceToWall;
GuiElementList timerProgressBars;
Beacon b;
enum switchable_elements { PUMP, MAGNETS, SENSORS, CAM1, CAM2, CAM3, CAM4 };

#include "handlers.hpp"

void buildGui() {
	
	//from top to bottom order
	//x,y,w,h,progress
	pbTakeOffTime = new GuiTimerProgressBar(10, 10, 780, 30, "Time elapsed since take off", 0,0,900);
	timerProgressBars.addChild(pbTakeOffTime);
	pbGlueTime = new GuiTimerProgressBar(10, 50, 780, 30, "Time elapsed since gluing", 0,0,3);
	//timerProgressBars.addChild(pbGlueTime);
	GuiButton* btnTakeOffTimer = new GuiButton(185, 90, 100, 60, "Take off");
	GuiButton* btnGlueTimer = new GuiButton(295, 90, 100, 60, "Glue");
	GuiButton* btnReleasePad = new GuiButton(405, 90, 100, 60, "Magnets");
	GuiButton* btnSuccion = new GuiButton(515, 90, 100, 60, "Pump");

	pbArmXAngle = new GuiProgressBar(250, 160, 300, 30, "Arm pan angle", 10);
	pbArmYAngle = new GuiProgressBar(250, 200, 300, 30, "Arm tilt angle", 50);
	pbDistanceToWall = new GuiProgressBar(250, 240, 300, 30, "Distance to wall", 20);

	GuiCheckBox* chkCamera1 = new GuiCheckBox(100, 280, 100, 30, "Cam 1");
	GuiCheckBox* chkCamera2 = new GuiCheckBox(200, 280, 100, 30, "Cam 2");
	GuiCheckBox* chkCamera3 = new GuiCheckBox(300, 280, 100, 30, "Cam 3");
	GuiCheckBox* chkCamera4 = new GuiCheckBox(400, 280, 100, 30, "Cam 4");
	GuiCheckBox* chkLiDAR = new GuiCheckBox(500, 280, 100, 30, "LiDAR");
	GuiCheckBox* chkAutoArm = new GuiCheckBox(600, 280, 100, 30, "Arm auto");

	GuiButton* btnPanTilt1Up = new GuiButton(120, 330, 100, 60, "Up");
	GuiButton* btnPanTilt1Left = new GuiButton(10, 365, 100, 60, "Left");
	GuiButton* btnPanTilt1Right = new GuiButton(230, 365, 100, 60, "Right");
	GuiButton* btnPanTilt1Down = new GuiButton(120, 400, 100, 60, "Down");
	GuiButton* btnPanTilt2Up = new GuiButton(580, 330, 100, 60, "Up");
	GuiButton* btnPanTilt2Left = new GuiButton(470, 365, 100, 60, "Left");
	GuiButton* btnPanTilt2Right = new GuiButton(690, 365, 100, 60, "Right");
	GuiButton* btnPanTilt2Down = new GuiButton(580, 400, 100, 60, "Down");

	gui.addChild(pbTakeOffTime);
	gui.addChild(pbGlueTime);
	gui.addChild(pbArmXAngle);
	gui.addChild(pbArmYAngle);
	gui.addChild(pbDistanceToWall);

	gui.addChild(btnTakeOffTimer);
	btnTakeOffTimer->connectCallback(btnTakeOffTimeHandler);
	gui.addChild(btnGlueTimer);
	btnGlueTimer->connectCallback(btnGlueTimeHandler);
	gui.addChild(btnReleasePad);
	btnReleasePad->connectCallback(btnReleasePadHandler);
	gui.addChild(btnSuccion);
	btnSuccion->connectCallback(btnSuccionHandler);

	gui.addChild(chkCamera1);
	chkCamera1->connectCallback(chkCamera1Handler);
	gui.addChild(chkCamera2);
	chkCamera2->connectCallback(chkCamera2Handler);
	gui.addChild(chkCamera3);
	chkCamera3->connectCallback(chkCamera3Handler);
	gui.addChild(chkCamera4);
	chkCamera4->connectCallback(chkCamera4Handler);
	gui.addChild(chkLiDAR);
	chkLiDAR->connectCallback(chkLiDARHandler);
	gui.addChild(chkAutoArm);
	chkAutoArm->connectCallback(chkAutoArmHandler);

	gui.addChild(btnPanTilt1Up);
	btnPanTilt1Up->connectCallback(btnPanTilt1UpHandler);
	gui.addChild(btnPanTilt1Left);
	btnPanTilt1Left->connectCallback(btnPanTilt1LeftHandler);
	gui.addChild(btnPanTilt1Right);
	btnPanTilt1Right->connectCallback(btnPanTilt1RightHandler);
	gui.addChild(btnPanTilt1Down);
	btnPanTilt1Down->connectCallback(btnPanTilt1DownHandler);
	gui.addChild(btnPanTilt2Up);
	btnPanTilt2Up->connectCallback(btnPanTilt2UpHandler);
	gui.addChild(btnPanTilt2Left);
	btnPanTilt2Left->connectCallback(btnPanTilt2LeftHandler);
	gui.addChild(btnPanTilt2Right);
	btnPanTilt2Right->connectCallback(btnPanTilt2RightHandler);
	gui.addChild(btnPanTilt2Down);
	btnPanTilt2Down->connectCallback(btnPanTilt2DownHandler);
}

uint32_t changeMillis = 0;
uint32_t pressMillis = 0;
uint32_t batteryupdatemillis = 0;
bool firstPress=true;
bool pressed=true;
#define BOUNCE_TIME 50
#define SHUTDOWN_HOLD_TIME 1000
void btnOnOff()
{
	if (!firstPress)
	{
		if (millis() > changeMillis && millis() - changeMillis > BOUNCE_TIME)
		{
			pressed = digitalRead(BTN_ON_OFF);
			Serial.print(pressed);
			Serial.print(" @ ");
			Serial.println(millis());

			if (pressed) {
				pressMillis = millis();
			}
			else checkShutdown();
		}

	}
	else
	{
		firstPress = false;
	}
	changeMillis = millis();
}
void setup(void) {

	Serial.begin(115200);

	pinMode(KEEP_ALIVE, OUTPUT);
	pinMode(BTN_ON_OFF, INPUT);
	pinMode(RA8875_CS, OUTPUT);
	digitalWrite(RA8875_CS,HIGH);
	digitalWrite(KEEP_ALIVE, HIGH);
	attachInterrupt(digitalPinToInterrupt(BTN_ON_OFF), btnOnOff, CHANGE);
	
	pinMode(RFM_RST, OUTPUT);
	digitalWrite(RFM_RST, LOW);

	Serial.println("Feather RFM69 RX Test!");
	Serial.println();

	// manual reset
	digitalWrite(RFM_RST, HIGH);
	delay(10);
	digitalWrite(RFM_RST, LOW);
	delay(10);
	
	if (!rf69.init())
		Serial.println("init failed");

	// Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
	// No encryption
	if (!rf69.setFrequency(868.0))
		Serial.println("setFrequency failed");

	// If you are using a high power RF69, you *must* set a Tx power in the
	// range 14 to 20 like this:
	rf69.setTxPower(14,true);

	// The encryption key has to be the same as the one in the server
	// 16 bytes
	uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 };
	rf69.setEncryptionKey(key);
  

	/* Initialise the display using 'RA8875_480x272' or 'RA8875_800x480' */
	if (!tft.begin(RA8875_800x480)) {
		Serial.println("RA8875 Not Found!");
		while (1);
	}
	Serial.println("Found RA8875");

	pinMode(RA8875_INT, INPUT);
	tft.setFont(&FreeSansBold9pt7b);
	tft.displayOn(true);
	tft.GPIOX(true);      // Enable TFT - display enable tied to GPIOX
	tft.PWM1config(true, RA8875_PWM_CLK_DIV1024); // PWM output for backlight
	tft.PWM1out(255);
	tft.touchEnable(true);
 
	GuiUtils::tsCalibrate(RA8875_INT);

	buildGui();
  
	gui.draw();

	b.en = 0xFF;
	b.panTilt = 0x00;
	b.req = 0x00;
}


void loop()
{
	//update timer progressBars regularly
	GuiTimerProgressBar::update(pbTakeOffTime);
	GuiTimerProgressBar::update(pbGlueTime);
	// this has to be called to keep the system up to date and catch touch input
	gui.update();

	checkShutdown();

	//every 50ms average
	if (millis() > batteryupdatemillis + 50)
	{
		batteryupdatemillis = millis();
		float measuredvbat = analogRead(VBATPIN);
		measuredvbat *= 2;    // we divided by 2, so multiply back
		measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
		measuredvbat /= 1024; // convert to voltage
		Serial.print("VBat: "); Serial.println(measuredvbat);

		//test for beacon radio sending every second

		uint8_t data[3] = { b.en, b.panTilt, b.req };
		rf69.send(data, 3);
		rf69.waitPacketSent();
		Serial.print("sent: "); Serial.println(b.en);
	}

}
void checkShutdown()
{
	//do we must shutdown the controller ?
	if (firstPress) return;
	if (!digitalRead(BTN_ON_OFF)) return;
	if (millis() > pressMillis && millis() - pressMillis > SHUTDOWN_HOLD_TIME)
	{
		//This will put the EN pin low on the Feather M0, therefore shutdown.
		tft.PWM1out(0);
		tft.displayOn(false);
		digitalWrite(KEEP_ALIVE, LOW);
	}
}