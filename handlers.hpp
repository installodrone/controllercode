uint8_t btnTakeOffTimeHandler(void *a, GuiElement *element, uint8_t event) 
{
	GuiTimerProgressBar::stateMachineUpdate(pbTakeOffTime, event);
};
uint8_t btnGlueTimeHandler(void *a, GuiElement *element, uint8_t event)
{
	GuiTimerProgressBar::stateMachineUpdate(pbGlueTime, event);
};
uint8_t btnReleasePadHandler(void *a, GuiElement *element, uint8_t event)
{
	//toggle this element on press
	if(event == GUI_EVENT_PRESS)
		b.en = (b.en ^ (0x01 << MAGNETS));
};
uint8_t btnSuccionHandler(void *a, GuiElement *element, uint8_t event)
{	
	//toggle this element on press
	if (event == GUI_EVENT_PRESS)
		b.en = (b.en ^ (0x01 << PUMP));
};

uint8_t btnPanTilt1UpHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t btnPanTilt1LeftHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t btnPanTilt1RightHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t btnPanTilt1DownHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t btnPanTilt2UpHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t btnPanTilt2LeftHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t btnPanTilt2RightHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t btnPanTilt2DownHandler(void *a, GuiElement *element, uint8_t event)
{};

uint8_t chkCamera1Handler(void *a, GuiElement *element, uint8_t event)
{
	//toggle this element on press
	if (event == GUI_EVENT_PRESS)
		b.en = (b.en ^ (0x01 << CAM1));
};
uint8_t chkCamera2Handler(void *a, GuiElement *element, uint8_t event)
{
	//toggle this element on press
	if (event == GUI_EVENT_PRESS)
		b.en = (b.en ^ (0x01 << CAM2));
};
uint8_t chkCamera3Handler(void *a, GuiElement *element, uint8_t event)
{
	//toggle this element on press
	if (event == GUI_EVENT_PRESS)
		b.en = (b.en ^ (0x01 << CAM3));
};
uint8_t chkCamera4Handler(void *a, GuiElement *element, uint8_t event)
{
	//toggle this element on press
	if (event == GUI_EVENT_PRESS)
		b.en = (b.en ^ (0x01 << CAM4));
};
uint8_t chkLiDARHandler(void *a, GuiElement *element, uint8_t event)
{};
uint8_t chkAutoArmHandler(void *a, GuiElement *element, uint8_t event)
{};

