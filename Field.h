#pragma once
#include <stdint.h>
class Field {
public:
	uint8_t header = 0;
	uint8_t* data = nullptr;
	uint8_t length = 0;
};
